# Symfony ORM
Projet symfony avec le web-skeleton et toute la puissance du framework

## How To use
1. Cloner le projet
2. Faire un `composer install`
3. Créer le .env.local avec la DATABASE_URL dedans (en se basant sur celle qu'il y a dans le .env)
4. (optionnel) Faire un `bin/console doctrine:database:create` pour créer la bdd si elle n'existe pas
5. Faire un `bin/console doctrine:migrations:migrate` pour exécuter les dernière migrations et mettre à jour la base de données

Si ça ne marche pas, faire un `bin/console doctrine:database:drop` et reprendre à l'étape 4