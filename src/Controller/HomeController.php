<?php

namespace App\Controller;

use App\Entity\Bike;
use App\Form\BikeType;
use App\Repository\BikeRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(BikeRepository $repo)
    {
        $bikes = $repo->findAll();
        return $this->render('home/index.html.twig', [
            'bikes' => $bikes
        ]);
    }
    /**
     * @Route("/add-bike", name="add_bike")
     */
    public function form(Request $request, ObjectManager $manager) {
        $bike = new Bike();
        $form = $this->createForm(BikeType::class, $bike);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($bike);
            $manager->flush();
        }

        return $this->render('home/form.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /*
    Cette route répète beaucoup de truc de la méthode add,
    il y aurait tout à fait moyen d'en faire une seule route
    pour les 2 actions en assignant 2 @Route à la même méthode 
    et en rajoutant un ou deux if-else
    */
    /**
     * @Route("/modify-bike/{id}", name="modify_bike")
     */
    public function modifyBike(Bike $bike, Request $request, ObjectManager $manager) {
        
        $form = $this->createForm(BikeType::class, $bike);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
        }

        return $this->render('home/modify-bike.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
