<?php

namespace App\Controller;

use App\Entity\Rental;
use App\Form\RentalType;
use App\Repository\BikeRepository;
use App\Repository\RentalRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RentalController extends AbstractController
{
    /**
     * @Route("/rent", name="rental")
     */
    public function index(RentalRepository $repo)
    {
        return $this->render('rental/index.html.twig', [
            'rentals' => $repo->findAll()
        ]);
    }
    /**
     * @Route("/rent/add", name="add_rental")
     */
    public function addForm(Request $request, ObjectManager $manager) {
        $rental = new Rental();
        $form = $this->createForm(RentalType::class, $rental);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($rental);
            $manager->flush();
            return $this->redirectToRoute('rental');
        }

        return $this->render('rental/form-add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/rental/{id}", name="one_rental")
     */
    public function oneRental(Rental $rental) {
        return $this->render('rental/one.html.twig', [
            'rental' => $rental
        ]);
    }
}
